using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControllManager : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] PlayerInput playerInput;
    [SerializeField] List<ControllScheme> avialableControllSchemes;

    public Camera MainCamera => mainCamera;

    public PlayerInput PlayerInput => playerInput;

    private ControllScheme currentScheme;
    public ControllScheme CurrentScheme => currentScheme;

    PlayerControls playerControls;
    public PlayerControls PlayerControls => playerControls;

    public bool AllSchemesInitialized
    {
        get
        {
            foreach(ControllScheme scheme in avialableControllSchemes)
            {
                if (!scheme.Initialized)
                    return false;
            }
            return true;
        }
    }

    private void Awake()
    {
        playerControls = new PlayerControls();
        foreach (ControllScheme scheme in avialableControllSchemes)
            scheme.PlayerControls = playerControls;
        playerInput.actions = playerControls.asset;
    }

    private void Start()
    {
        ChangeSchemeType(ControllSchemeType.Ground).Forget();
    }

    public async UniTask ChangeSchemeType(ControllSchemeType changeTo)
    {
        await UniTask.WaitUntil(()=>AllSchemesInitialized);
        foreach(ControllScheme scheme in avialableControllSchemes)
        {
            if (scheme.GetSchemeType() == changeTo)
            {
                scheme.InputMap.Enable();
                currentScheme = scheme;
            }
            else
                scheme.InputMap.Disable();
        }
    }
}
