using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public static string scene_mainControllScene = "UI";

    public static string FastDragonMainScene = "FastDragon";
    public static string OrdinaryDragonMainScene = "StandardDragon";
    public static string StrongDragonMainScene = "MeleeDragon";
    public static string[] FastDragonSceneSet = { "FastDragon" };
    public static string[] OrdinaryDragonSceneSet = { "StandardDragon" };
    public static string[] StrongDragonSceneSet = { "MeleeDragon" };
}
