using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class SceneLoader
{
    readonly ZenjectSceneLoader _sceneLoader;

    public Action OnChangeScene;

    public SceneLoader(ZenjectSceneLoader sceneLoader)
    {
        _sceneLoader = sceneLoader;
    }

    private List<AsyncOperation> asyncOperation = new List<AsyncOperation>();

    private string[] currentActiveScenes;
    private string currentActiveMainScene;
    private bool isBusy;

    public async UniTask UnloadScenes()
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (isBusy == true)
            {
                Debug.LogError($"is busy by unloading scenes!");
                return;
            }
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name != Globals.scene_mainControllScene)
            {
                asyncOperation.Add(SceneManager.UnloadSceneAsync(scene));
            }
        }

        isBusy = true;
        foreach (AsyncOperation async in asyncOperation)
        {
            await async;
        }

        asyncOperation.Clear();
        isBusy = false;
    }

    public async UniTask ReloadScenes()
    {
        OnChangeScene?.Invoke();
        await LoadSceneAndSetSceneActive(currentActiveScenes, currentActiveMainScene);
    }

    public async UniTask LoadSceneAndSetSceneActive(string[] LvlNames, string activeScene)
    {
        Debug.Log("Loading scene" + LvlNames[0]);
        OnChangeScene?.Invoke();
        currentActiveScenes = LvlNames;
        currentActiveMainScene = activeScene;

        for (int i = 0; i < LvlNames.Length; i++)
        {
            if (isBusy)
            {
                Debug.LogError($" is busy by loading scenes!");
                return;
            }
            asyncOperation.Add(_sceneLoader.LoadSceneAsync(LvlNames[i], LoadSceneMode.Additive));
        }

        isBusy = true;
        foreach (AsyncOperation async in asyncOperation)
        {
            await async;
        }

        var mainScene = SceneManager.GetSceneByName(activeScene);

        SceneManager.SetActiveScene(mainScene);
        asyncOperation.Clear();
        LightProbes.Tetrahedralize();
        isBusy = false;
    }

    public async UniTask UnloadScene(string sceneName)
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (isBusy == true)
            {
                Debug.LogError($"is busy by unloading scenes!");
                await UniTask.WaitUntil(() => !isBusy);
                //return;
            }
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name.Equals(sceneName))
            {
                asyncOperation.Add(SceneManager.UnloadSceneAsync(scene));
            }
        }

        isBusy = true;
        foreach (AsyncOperation async in asyncOperation)
        {
            await async;
        }

        asyncOperation.Clear();
        isBusy = false;
    }

    public async UniTask UnloadScenes(string[] sceneNames)
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (isBusy == true)
            {
                Debug.LogError($"is busy by unloading scenes!");
                return;
            }
            Scene scene = SceneManager.GetSceneAt(i);
            foreach (string sceneName in sceneNames)
            {
                if (scene.name.Equals(sceneName))
                {
                    asyncOperation.Add(SceneManager.UnloadSceneAsync(scene));
                }
            }

        }

        isBusy = true;
        foreach (AsyncOperation async in asyncOperation)
        {
            await async;
        }

        asyncOperation.Clear();
        isBusy = false;
    }
}
