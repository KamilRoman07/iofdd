using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "MonsterSpawnDataBase", menuName = "Data/Enemy/MonsterSpawnDataBase", order = 1)]
    public class MonsterSpawnDataBase : ScriptableObject
    {
        [SerializeField] int spawnTime;
        [SerializeField] private GameObject prefab;

        public GameObject Prefab => prefab;
        public int SpawnTime => spawnTime;
    }
}
