using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ChooseDragonPanelController : MonoBehaviour
{
    [Inject] SceneLoader sceneLoader;

    [SerializeField] GameObject panel;

    //0 - fast, 1 - medium, 2 - strong
    public void SpawnDragon(int index)
    {
        if (sceneLoader != null)
        {
            switch (index)
            {
                case 0:
                    sceneLoader.LoadSceneAndSetSceneActive(Globals.FastDragonSceneSet, Globals.FastDragonMainScene).Forget();
                    break;
                case 1:
                    sceneLoader.LoadSceneAndSetSceneActive(Globals.OrdinaryDragonSceneSet, Globals.OrdinaryDragonMainScene).Forget();
                    break;
                case 2:
                    sceneLoader.LoadSceneAndSetSceneActive(Globals.StrongDragonSceneSet, Globals.StrongDragonMainScene).Forget();
                    break;
                default:
                    sceneLoader.LoadSceneAndSetSceneActive(Globals.OrdinaryDragonSceneSet, Globals.OrdinaryDragonMainScene).Forget();
                    break;
            }

            panel.SetActive(false);
        }
    }
}
