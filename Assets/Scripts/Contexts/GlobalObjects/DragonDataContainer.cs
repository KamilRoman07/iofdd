using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonDataContainer
{
    public bool jumping = false;
    public bool sneaking = false;
    public float movementSpeed = 3f;
}
