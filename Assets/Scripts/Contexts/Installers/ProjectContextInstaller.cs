using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ProjectContextInstaller : MonoInstaller<ProjectContextInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<DragonDataContainer>().AsSingle();
        Container.BindInterfacesAndSelfTo<SceneLoader>().AsSingle();
    }
}
