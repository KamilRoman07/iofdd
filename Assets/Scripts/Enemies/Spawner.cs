using Cysharp.Threading.Tasks;
using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private int maxSpawnedMonsters;
    [SerializeField] private MonsterSpawnDataBase spawnedMonster;
    private List<MonsterSpawnDataBase> spawnedMonsters;

    private void Awake()
    {
        spawnedMonsters = new List<MonsterSpawnDataBase>();
        //LoadData();
        StartCoroutine(SpawnMonster(spawnedMonster.SpawnTime));
    }

    private IEnumerator SpawnMonster(int spawnTime)
    {
        yield return new WaitForSeconds(spawnTime);

        StartCoroutine(SpawnMonster(spawnTime));
        if (spawnedMonsters.Count < maxSpawnedMonsters)
            SpawnMonster().Forget();
    }

    private async UniTask SpawnMonster()
    {

    }
}
