using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class GroundControls : ControllScheme
{
    protected override ControllSchemeType schemeType => ControllSchemeType.Ground;

    protected override void SetControlls()
    {
        playerControls.GroundControls.Move.performed += ReadMovement;
        playerControls.GroundControls.Move.canceled += ReadMovement;
        playerControls.GroundControls.Jump.performed += Jump;
        playerControls.GroundControls.Sneak.performed += Sneak;
    }

    protected override void DestroyControlls()
    {
        playerControls.GroundControls.Move.performed -= ReadMovement;
        playerControls.GroundControls.Move.canceled -= ReadMovement;
        playerControls.GroundControls.Jump.performed -= Jump;
        playerControls.GroundControls.Sneak.performed -= Sneak;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ground"))
            dragonDataContainer.jumping = false;
    }

    private void Sneak(InputAction.CallbackContext obj)
    {
        if (obj.performed)
            dragonDataContainer.sneaking = true;

        if (obj.canceled)
            dragonDataContainer.sneaking = false;
    }

    private void Jump(InputAction.CallbackContext obj)
    {
        if (obj.performed)
        {
            if (!dragonDataContainer.jumping)
            {
                dragonDataContainer.jumping = true;
                vSpeed = 5;
            }
            else
            {
                //TODO add flying check
                dragonDataContainer.jumping = false;
                controllManager.ChangeSchemeType(ControllSchemeType.Flying).Forget();
            }
        }
    }

    private void ReadMovement(InputAction.CallbackContext obj)
    {
        if (obj.performed && !dragonDataContainer.jumping)
        {
            Vector2 movementInput = -obj.ReadValue<Vector2>();
            movementVector = new Vector3(movementInput.x, vSpeed, movementInput.y);
        }

        if (obj.canceled && !dragonDataContainer.jumping)
            movementVector = new Vector3(0, vSpeed, 0);
    }

    protected override void Move()
    {
        speed = dragonDataContainer.movementSpeed * (dragonDataContainer.sneaking ? 0.5f : 1f);
        base.Move();

        if (!dragonDataContainer.jumping)
            vSpeed = 0;
        else
            vSpeed -= 9.81f * Time.deltaTime;
    }
}
