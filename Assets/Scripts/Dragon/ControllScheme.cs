using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class ControllScheme : MonoBehaviour
{
    [Inject] protected DragonDataContainer dragonDataContainer;

    [SerializeField] string mapKey;

    protected CharacterController playerController;
    protected ControllManager controllManager;
    protected Vector3 movementVector = Vector3.zero;
    protected float speed;
    protected float vSpeed = 0;

    private bool initialized = false;
    public bool Initialized => initialized;

    private InputActionMap inputMap;
    public InputActionMap InputMap => inputMap;

    protected PlayerControls playerControls;
    public PlayerControls PlayerControls
    {
        get
        {
            return playerControls;
        }
        set
        {
            playerControls = value;
        }
    }

    public string MapKey => mapKey;

    protected virtual ControllSchemeType schemeType => ControllSchemeType.Default;

    public ControllSchemeType GetSchemeType()
    {
        return schemeType;
    }

    public virtual void Awake()
    {
        if (!playerController) gameObject.TryGetComponent(out playerController);
        if (!controllManager) gameObject.TryGetComponent(out controllManager);
        SetControllsAsync().Forget();
    }

    private void Start()
    {
        speed = dragonDataContainer.movementSpeed;
    }

    private void Update()
    {
        if (controllManager.CurrentScheme == this)
            Move();
    }

    public virtual void OnDestroy()
    {
        SetDestroyControllsAsyn().Forget();
    } 

    private async UniTask SetControllsAsync()
    {
        await UniTask.WaitUntil(()=> playerControls != null);
        inputMap = controllManager.PlayerInput.actions.FindActionMap(mapKey);
        SetControlls();
        initialized = true;
    }    

    protected virtual void SetControlls()
    {

    }
    
    private async UniTask SetDestroyControllsAsyn()
    {
        await UniTask.WaitUntil(() => playerControls != null);
        DestroyControlls();
    }

    protected virtual void DestroyControlls()
    {

    }

    protected virtual void Move() 
    {
        movementVector = new Vector3(movementVector.x, vSpeed, movementVector.z);
        playerController.Move(transform.rotation * movementVector * Time.deltaTime * speed);
    }
}
