using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DragonCameraControlls : MonoBehaviour
{
    [SerializeField] CharacterController characterController;
    [SerializeField] CinemachineFreeLook cinemachineFreeLook;
    [SerializeField] ControllManager controllManager;

    [SerializeField] float xSensitivity = 0.1f;
    [SerializeField] float ySensitivity = 0.03f;

    private Vector2 lookDelta;
    private float zoomValue;
    private bool rotateCamera;
    private bool moveCharacter;
    

    private void Start()
    {
        controllManager.PlayerControls.GroundControls.Look.performed += OnLook;
        controllManager.PlayerControls.FlyingControls.Look.performed += OnLook;
        controllManager.PlayerControls.GroundControls.Look.canceled += OnLook;
        controllManager.PlayerControls.FlyingControls.Look.canceled += OnLook;

        controllManager.PlayerControls.GroundControls.LookMoveCamera.performed += StartRotating;
        controllManager.PlayerControls.GroundControls.LookMoveCharacter.performed += StartRotatingMoveCharacter;

        controllManager.PlayerControls.FlyingControls.LookMoveCamera.performed += StartRotating;
        controllManager.PlayerControls.FlyingControls.LookMoveCharacter.performed += StartRotatingMoveCharacter;

        controllManager.PlayerControls.GroundControls.LookMoveCamera.canceled += StopRotating;
        controllManager.PlayerControls.GroundControls.LookMoveCharacter.canceled += StopRotatingMoveCharacter;

        controllManager.PlayerControls.FlyingControls.LookMoveCamera.canceled += StopRotating;
        controllManager.PlayerControls.FlyingControls.LookMoveCharacter.canceled += StopRotatingMoveCharacter;

        controllManager.PlayerControls.FlyingControls.Zoom.performed += Zoom;
        controllManager.PlayerControls.FlyingControls.Zoom.canceled += Zoom;
        controllManager.PlayerControls.GroundControls.Zoom.performed += Zoom;
        controllManager.PlayerControls.GroundControls.Zoom.canceled += Zoom;
    }

    private void Update()
    {
        if (rotateCamera)
            RotateCamera(lookDelta.x, lookDelta.y);

        if (moveCharacter)
            MoveCharacter();

        ZoomCamera();
    }

    private void OnDestroy()
    {

        controllManager.PlayerControls.GroundControls.Look.performed -= OnLook;
        controllManager.PlayerControls.FlyingControls.Look.performed -= OnLook;
        controllManager.PlayerControls.GroundControls.Look.canceled -= OnLook;
        controllManager.PlayerControls.FlyingControls.Look.canceled -= OnLook;

        controllManager.PlayerControls.GroundControls.LookMoveCamera.performed -= StartRotating;
        controllManager.PlayerControls.GroundControls.LookMoveCharacter.performed -= StartRotatingMoveCharacter;

        controllManager.PlayerControls.FlyingControls.LookMoveCamera.performed -= StartRotating;
        controllManager.PlayerControls.FlyingControls.LookMoveCharacter.performed -= StartRotatingMoveCharacter;

        controllManager.PlayerControls.GroundControls.LookMoveCamera.canceled -= StopRotating;
        controllManager.PlayerControls.GroundControls.LookMoveCharacter.canceled -= StopRotatingMoveCharacter;

        controllManager.PlayerControls.FlyingControls.LookMoveCamera.canceled -= StopRotating;
        controllManager.PlayerControls.FlyingControls.LookMoveCharacter.canceled -= StopRotatingMoveCharacter;
    }

    #region Read input
    private void StartRotating(InputAction.CallbackContext c)
    {
        rotateCamera = true;
    }    
    
    private void StartRotatingMoveCharacter(InputAction.CallbackContext c)
    {
        rotateCamera = true;
        moveCharacter = true;
    }

    private void StopRotating(InputAction.CallbackContext c)
    {
        rotateCamera = false;
    }
    private void StopRotatingMoveCharacter(InputAction.CallbackContext c)
    {
        rotateCamera = false;
        moveCharacter = false;
    }

    private void OnLook(InputAction.CallbackContext c)
    {
        lookDelta = c.ReadValue<Vector2>();
    }

    private void Zoom(InputAction.CallbackContext c)
    {
        zoomValue = c.ReadValue<Vector2>().y;
    }
    #endregion

    private void RotateCamera(float xAxisFactor, float yAxisFactor)
    {
        if (cinemachineFreeLook)
        {
            cinemachineFreeLook.m_XAxis.Value = -xAxisFactor * xSensitivity;
            cinemachineFreeLook.m_YAxis.Value = Mathf.Clamp(cinemachineFreeLook.m_YAxis.Value + yAxisFactor * ySensitivity * 0.01f, 0f, 1f);
        }
    }
    
    private void ZoomCamera()
    {
        if (cinemachineFreeLook)
        {
            cinemachineFreeLook.m_Orbits[0].m_Height = Mathf.Clamp(cinemachineFreeLook.m_Orbits[0].m_Height + -zoomValue * 0.01f, 4f, 14f);
            cinemachineFreeLook.m_Orbits[1].m_Radius = Mathf.Clamp(cinemachineFreeLook.m_Orbits[1].m_Radius + -zoomValue * 0.01f, 5f, 15f);
            cinemachineFreeLook.m_Orbits[2].m_Height = -Mathf.Clamp(cinemachineFreeLook.m_Orbits[0].m_Height + -zoomValue * 0.01f, 4f, 14f);
        }
    }

    private void MoveCharacter()
    {
        Vector3 rot = controllManager.MainCamera.transform.eulerAngles + new Vector3(0,180,0);
        if (controllManager.CurrentScheme.GetSchemeType() != ControllSchemeType.Flying)
        {
            rot.x = controllManager.transform.eulerAngles.x;
            rot.z = controllManager.transform.eulerAngles.z;
        }
        else
        {
            rot.x = -rot.x;
            rot.z = -rot.z;
        }

        controllManager.transform.eulerAngles = rot;
    }
}
