using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControllSchemeType
{
    Default,
    Ground,
    Flying,
    Swimming,
}
