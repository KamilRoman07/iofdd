using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FlyingControls : ControllScheme
{
    protected override ControllSchemeType schemeType => ControllSchemeType.Flying;

    protected override void SetControlls()
    {
        playerControls.FlyingControls.Move.performed += ReadMovement;
        playerControls.FlyingControls.Move.canceled += ReadMovement;
        playerControls.FlyingControls.Ascend.performed += Ascend;
        playerControls.FlyingControls.Descent.performed += Descent;
        playerControls.FlyingControls.Ascend.canceled += Ascend;
        playerControls.FlyingControls.Descent.canceled += Descent;
    }

    protected override void DestroyControlls()
    {
        playerControls.FlyingControls.Move.performed -= ReadMovement;
        playerControls.FlyingControls.Move.canceled -= ReadMovement;
        playerControls.FlyingControls.Ascend.performed -= Ascend;
        playerControls.FlyingControls.Descent.performed -= Descent;
        playerControls.FlyingControls.Ascend.canceled -= Ascend;
        playerControls.FlyingControls.Descent.canceled -= Descent;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ground"))
            controllManager.ChangeSchemeType(ControllSchemeType.Ground).Forget();
    }

    private void Descent(InputAction.CallbackContext obj)
    {
    }

    private void Ascend(InputAction.CallbackContext obj)
    {
    }

    private void ReadMovement(InputAction.CallbackContext obj)
    {
        if (obj.performed)
        {
            Vector2 movementInput = -obj.ReadValue<Vector2>();
            movementVector = new Vector3(movementInput.x, 0, movementInput.y);
        }

        if (obj.canceled)
            movementVector = Vector3.zero;
    }

    protected override void Move()
    {
        speed = dragonDataContainer.movementSpeed;
        base.Move();
    }
}
